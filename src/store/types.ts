export interface RootState {
  version: string;
  electionState?: ElectionState;
}

export interface Election {
  id: string;
  name: string;
  electionDay: string;
  ocdDivisionId: string;
}

export interface ElectionState {
  elections: Election[] | undefined;
  error: string | undefined;
}