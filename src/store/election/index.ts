import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { RootState, ElectionState } from '../types';

export const state: ElectionState = {
    elections: undefined,
    error: undefined  
}

const namespaced: boolean = true;

export const election: Module<ElectionState, RootState> = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};